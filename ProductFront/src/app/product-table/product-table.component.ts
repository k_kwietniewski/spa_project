import { Component, OnInit } from '@angular/core';
import { ProductService } from "../product.service";
import { ProductsEntity } from '../product';
import { MatTableDataSource } from "@angular/material/table";

@Component({
  selector: 'app-product-table',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.css']
})
export class ProductTableComponent implements OnInit {

  displayedColumns: string[];
  dataSource;

  constructor(private productService: ProductService) { }
  ngOnInit(): void {
    this.getProducts();
    this.displayedColumns = ['btnDelete', 'id', 'nazwa', 'cena', 'waga', 'producent', 'btnEdit'];
  }
  
  products: ProductsEntity[];
  
  getProducts(): void{
    this.productService.getProducts()
    .subscribe(products=>{this.products = products;
                          this.dataSource = new MatTableDataSource(products)});
  }

  delete(product: ProductsEntity): void{
    this.products = this.products.filter(s=>s.id !== product.id);
    this.productService.deleteProduct(product).subscribe();
    window.location.reload();
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }
}
