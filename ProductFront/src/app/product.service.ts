import { Injectable } from '@angular/core';
import { ProductsEntity } from "./product";
import {Observable} from 'rxjs';
// import {of} from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
 };

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  private productsApiUrl = 'http://localhost:5000/api/products';

  constructor(private http: HttpClient) { }
  getProducts(): Observable<ProductsEntity[]>{
    return this.http.get<ProductsEntity[]>(this.productsApiUrl)
  }

  getProduct(id: number): Observable<ProductsEntity>{
    const url = `${this.productsApiUrl}/${id}`;
    return this.http.get<ProductsEntity>(url);
  }

  updateProduct(product: ProductsEntity): Observable<any> {
    const url = `${this.productsApiUrl}/${product.id}`;
    return this.http.put(url, product, httpOptions);
  }

  createProduct(product: ProductsEntity): Observable<ProductsEntity> {
    return this.http.post<ProductsEntity>(this.productsApiUrl, product,
    httpOptions);
  }

  deleteProduct(product: ProductsEntity | number): Observable<ProductsEntity> {
    const id = typeof product === 'number' ? product : product.id;
    const url = `${this.productsApiUrl}/${id}`;
    return this.http.delete<ProductsEntity>(url, httpOptions);
  }
   
}
