import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductComponent } from "./product/product.component";
import { ProductTableComponent } from "./product-table/product-table.component";
import { ProductEditComponent } from "./product-edit/product-edit.component";


const routes: Routes = [
  { path: '', redirectTo: 'productTable', pathMatch: 'full' },
  {path: 'addProduct', component:ProductComponent},
  {path: 'productTable', component:ProductTableComponent},
  {path: 'editProduct/:id', component:ProductEditComponent}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
