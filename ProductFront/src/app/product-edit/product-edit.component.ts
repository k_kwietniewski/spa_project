import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { ProductService } from '../product.service';
import { ProductsEntity } from '../product';

@Component({
  selector: 'app-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.css']
})
export class ProductEditComponent implements OnInit {
  product: ProductsEntity;

  constructor(private route: ActivatedRoute, private productService: ProductService, private location: Location) { }

  ngOnInit(): void {
    this.getProduct();
  }

  getProduct(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.productService.getProduct(id)
    .subscribe(product => this.product = product);
  }

  goBack(): void{
    this.location.back();
  }

  createAlert(): HTMLDivElement{
    let div = document.createElement('div');
    div.classList.add('alert');
    div.classList.add('alert-warning');
    div.textContent = "Uzupełnij wszystkie pola!";
    div.style.backgroundColor = "#f36257";
    div.style.color = "white";
    div.style.borderColor = "#f36257";
    div.style.fontWeight = "500";
    div.style.textAlign = "center";
    div.style.marginTop = "2%";
    div.style.display = "none";

    return div;
  }

  update(nazwa: string, cena: string, waga: string, producent: string): void{
    if(nazwa != '' && cena != '' && waga != '' && producent != ''){
      this.productService.updateProduct(this.product)
      .subscribe(()=>this.goBack());
    }else{
      let alert = this.createAlert();
      // console.log(this.createDiv());
      alert.style.display= "block";
      if(document.querySelector('.alert') == null){
        document.querySelector('.container').appendChild(alert);
      }
    }
  }
}
