import { Component, OnInit } from '@angular/core';
import { ProductService } from "../product.service";
import { ProductsEntity } from '../product';
import { Router } from '@angular/router';



@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})


export class ProductComponent implements OnInit {

  constructor(private productService: ProductService, public router: Router) { }
  
  ngOnInit(): void {
  }

  products: ProductsEntity[];

  createAlert(): HTMLDivElement{
    let div = document.createElement('div');
    div.classList.add('alert');
    div.classList.add('alert-warning');
    div.textContent = "Uzupełnij wszystkie pola!";
    div.style.backgroundColor = "#f36257";
    div.style.color = "white";
    div.style.borderColor = "#f36257";
    div.style.fontWeight = "500";
    div.style.textAlign = "center";
    div.style.marginTop = "2%";
    div.style.display = "none";

    return div;
  }
  create(nazwa: string, cena: string, waga: string, producent: string): void{
    if(nazwa != '' && cena != '' && waga != '' && producent != ''){
      this.productService.createProduct({nazwa: nazwa, cena:cena, waga:waga, producent: producent} as ProductsEntity)
      .subscribe(product => {this.products.push(product);});

      this.router.navigate(['/']);
    }
    else{
      let alert = this.createAlert();
      // console.log(this.createDiv());
      alert.style.display= "block";
      if(document.querySelector('.alert') == null){
        document.querySelector('.container').appendChild(alert);
      }
    }
  }
}
