﻿using Microsoft.EntityFrameworkCore;
using ProductApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi.Context
{
    public class ProductsDbEntities : DbContext
    {
        public ProductsDbEntities(DbContextOptions options) : base(options)
        {

        }
        public ProductsDbEntities()
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ProductsEntity>()
                .ToTable("Product");
            modelBuilder.Entity<ProductsEntity>()
                .HasKey(pk=>pk.Id);

        }

        public DbSet<ProductsEntity> Products { get; set; }
    }
}
