﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using ProductApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi.Context
{
    public class DesignTimeDbContextFactory : IDesignTimeDbContextFactory<ProductsDbEntities>
    {
        public ProductsDbEntities CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<ProductsDbEntities>();

            var connectionstring = @"Data Source=Products.db;";
            builder.UseSqlite(connectionstring);

            return new ProductsDbEntities(builder.Options);
        }
    }
}
