﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi.Models
{
    public class ProductsEntity
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(30)]
        public string Nazwa { get; set; }
        [Required]
        [MaxLength(10)]
        public string Cena { get; set; }
        [Required]
        [MaxLength(10)]
        public string Waga { get; set; }
        [Required]
        [MaxLength(30)]
        public string Producent { get; set; }
    }
}
