﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ProductApi.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProductApi
{
    public class DatabaseConnection
    {
        public static ProductsDbEntities Db()
        {
            var services = new ServiceCollection();
            services.AddDbContext<ProductsDbEntities>(factory =>
            {
                var conntectionstring = @"Data Source=Products.db;";
                factory.UseSqlite(conntectionstring);
            });

            var provider = services.BuildServiceProvider();

            var db = provider.GetService<ProductsDbEntities>();
            return db;

        }
    }
}
