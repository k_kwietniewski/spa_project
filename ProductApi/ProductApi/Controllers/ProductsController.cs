﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using ProductApi.Context;
using ProductApi.Models;

namespace ProductApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ProductsDbEntities _context;

        public ProductsController(ProductsDbEntities context)
        {
            _context = context;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ProductsEntity>>> GetProducts()
        {
            //LoadDefaultProducts();
            return await _context.Products.ToListAsync();
        }

        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ProductsEntity>> GetProductsEntity(int id)
        {
            var productsEntity = await _context.Products.FindAsync(id);

            if (productsEntity == null)
            {
                return NotFound();
            }

            return productsEntity;
        }

        // PUT: api/Products/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutProductsEntity(int id, ProductsEntity productsEntity)
        {
            if (id != productsEntity.Id)
            {
                return BadRequest();
            }

            _context.Entry(productsEntity).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductsEntityExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Products
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<ProductsEntity>> PostProductsEntity(ProductsEntity productsEntity)
        {
            _context.Products.Add(productsEntity);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProductsEntity", new { id = productsEntity.Id }, productsEntity);
        }

        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<ProductsEntity>> DeleteProductsEntity(int id)
        {
            var productsEntity = await _context.Products.FindAsync(id);
            if (productsEntity == null)
            {
                return NotFound();
            }

            _context.Products.Remove(productsEntity);
            await _context.SaveChangesAsync();

            return productsEntity;
        }

        private bool ProductsEntityExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
